Overview

This is a development POC and hence is not very straightforward to get to execute. The purpose of this demo is to reveal the different components and steps needed to reach a working Android NIF, to be used with Android userver. Following text explains how to get the demo to work, and hat to expect from it.

This text is divided into following steps:

1. Setup android build system
2. Setup AOSP build tree
3. Modify AOSP to include demo code
4. Build AOSP, with demo code
5. flash ROM into device
6. Test procedure

If you already have AOSP build setup, you can skip 1 and 2



1. Setup android build system
==============================

You'd need a linux PC running 64 bit os. My setup was Ubuntu 14.04 x86-64 with 8 GB RAM.


Install various android build requirements:

$ sudo apt-get update
$ sudo apt-get install openjdk-7-jdk
$ sudo apt-get install git-core gnupg flex bison gperf build-essential \
  zip curl zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386 \
  lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z-dev ccache \
  libgl1-mesa-dev libxml2-utils xsltproc unzip

Configure USB access to avoid using root all the time:

$ wget -S -O - http://source.android.com/source/51-android.rules | sed "s/<username>/$USER/" | sudo tee >/dev/null /etc/udev/rules.d/51-android.rules; sudo udevadm control --reload-rules

Specify output directories for AOSP build. Put this line in your ~/.bashrc:

export OUT_DIR_COMMON_BASE=<path-to-your-out-directory>  


Ref: https://source.android.com/source/initializing.html



2. Setup AOSP build tree
========================

This step will depend on your exact target. My target was Nexus 7 - 2013 tablet (flo). For this device, latest driver releases were in version LMY48M (branch: android-5.1.1_r14)

Ref: Google release tags: https://source.android.com/source/build-numbers.html#source-code-tags-and-builds

Add to your ~/.bashrc:
PATH=~/bin:$PATH

Install repo client in "your" bin dir:
$ mkdir ~/bin
$ curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
$ chmod a+x ~/bin/repo

make and cd to a working directory:

$ mkdir android-src-5.1.1_r14
$ cd android-src-5.1.1_r14

initialize repo:

$ repo init -u https://android.googlesource.com/platform/manifest -b android-5.1.1_r14

Download android source tree

$ repo sync

Downloading takes a LOT of time depending on your internet speed. This is 60+ GB of git clone.

Ref: https://source.android.com/source/downloading.html


Now, setup drivers for your device. Drivers for Google devices are here: https://developers.google.com/android/nexus/drivers

Create a directory "vendor" in root of AOSP. Unpack all driver files into vendor directory.



Remaining steps are w.r. to android source version 5.1.1_r14 for a Nexus 7-2013 tablet. Other android versions may have slight differences.

3. Modify AOSP to include demo code
===================================

Copy the directory uei to vendor directory

$ mv uei ~/android-src-5.1.1_r14/vendor/

Append the following lines to android-src-5.1.1_r14/device/asus/flo/BoardConfig.mk

BOARD_SEPOLICY_DIRS += \
vendor/uei/sepolicy

BOARD_SEPOLICY_UNION += \
device.te \
ueihidservice.te \
service_contexts \
file_contexts

Append the following lines to vedor/asus/flo/device-vendor.mk:

$ (call inherit-product-if-exists, vendor/uei/$(LOCAL_STEM))

Append the contents of uei/init.flo.rc to android-src-5.1.1_r14/device/asus/flo/init.flo.rc


4. Build AOSP, with demo code

$ . ./build/envsetup.sh
$ lunch aosp_flo-userdebug
$ make -j 16

Building a fresh repo took ~17 hours on my machine. YMMV

5. flash ROM into device

/* TODO */

$ adb reboot bootloader
$ fastboot -w all

6. Test procedure

/* TODO */

Setup android studio (or your favrite logcat viewer) to filter logs with "UEIHIDSRVC" tag

Pair and connect a bluetooth remote with tablet.

After connecting remote, all inputs on remote will generate logs showing received HID reports. This will also show any vendor-specific reports received by android device.
