#include <stdio.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <android/log.h>

#define LOGV(TAG,...) __android_log_print(ANDROID_LOG_VERBOSE, TAG,__VA_ARGS__)
#define LOGD(TAG,...) __android_log_print(ANDROID_LOG_DEBUG  , TAG,__VA_ARGS__)
#define LOGI(TAG,...) __android_log_print(ANDROID_LOG_INFO   , TAG,__VA_ARGS__)
#define LOGW(TAG,...) __android_log_print(ANDROID_LOG_WARN   , TAG,__VA_ARGS__)
#define LOGE(TAG,...) __android_log_print(ANDROID_LOG_ERROR  , TAG,__VA_ARGS__)

FILE *flogf		= NULL;

int openraw(char *path)
{
  int fd = -1;

//  fd = open(path, O_RDWR | O_CLOEXEC | O_NOATIME | O_NOCTTY | O_DIRECT);
  fd = open(path, O_RDWR);

  if(fd < 0)
  {
    LOGE("UEIHIDSRVC", "%s(): open(\"%s\") failed!!\n", __FUNCTION__, path);
    fprintf(flogf, "%s(): open(\"%s\") failed!!\n", __FUNCTION__, path);
    return -1;
  }
  else
  {
    LOGE("UEIHIDSRVC", "%s(): open(\"%s\") successful!!\n", __FUNCTION__, path);
    fprintf(flogf, "%s(): open(\"%s\") successful!!\n", __FUNCTION__, path);
  }

  return fd;
}


void printreport(uint8_t *report, unsigned int len)
{
  unsigned int i = 0;
  char		logbuf[256];
  unsigned int index		= 0;

  memset(logbuf, 0, sizeof(logbuf));

  
  if(len)
  {
    index	+= sprintf(&logbuf[index], "Rpt: 0x%02X, Data:", report[0]);

    for(i = 1; i < len; i++)
    {
        index	+= sprintf(&logbuf[index], " %02X", report[i]);
    }
    index	+= sprintf(&logbuf[index], " \n");
  }

  LOGE("UEIHIDSRVC", logbuf);
  fprintf(flogf, logbuf);
  fprintf(stdout, logbuf);

  LOGE("UEIHIDSRVC", "Report Rcvd");
  fprintf(flogf, "Report Rcvd");
}

int main(int carg, char **varg)
{
  unsigned int		count		= 0;
  ssize_t		rlen		= 0;
  int 			fd		= -1;;

  uint8_t		readbuf[256];

  char			*fpath		= "/dev/hidraw0";

  fprintf(stdout, "to stdout\n");
  fprintf(stderr, "to stderr\n");

  if(carg > 1)
  {
    fpath = varg[1];
  }

#if 1
  flogf		= stdout;
#else
  flogf = fopen("/var/fixpath/ueilog.txt", "w+");
  if(NULL == flogf)
  {
      flogf         = stdout;
      LOGE("UEIHIDSRVC", "Open logfile failed!!\n");
      fprintf(flogf, "Open logfile failed!!!!\n");
  }
#endif

  LOGE("UEIHIDSRVC", "Starting!!\n");
  fprintf(flogf, "Starting!!\n");

  while(1)
  {
    if(fd < 0)
    {
      count++;
      usleep(5000000);
      fd = openraw(fpath);

      LOGE("UEIHIDSRVC", "count = %d, fd = %d\n", count, fd);
      fprintf(flogf, "count = %d, fd = %d\n", count, fd);
    }
    else
    {
      rlen = read(fd, readbuf, sizeof(readbuf));

      if(rlen <= 0)
      {
        LOGE("UEIHIDSRVC", "Error reading. Closing!\n");
	fprintf(flogf, "Error reading. Closing!\n");
	close(fd);
      }
      else
      {
          //LOGE("UEIHIDSRVC", "");
	printreport(readbuf, rlen);
      }
    }
  }

  return 0;
}
